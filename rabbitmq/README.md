# usagi-cluster
> 🐇 Cluster for Swarm mode

RabbitMQ cluster for Docker Swarm mode

## Deploy the cluster
There are only 3 rabbitmq services defined in usagi.yml (that's cause of a 18.09 [bug](https://github.com/docker/for-linux/issues/375) so replicas don't work
```sh
# Create a Docker network for prod services
docker network create -d overlay --attachable backend

### Only if creating a cluster

# Add labels to the Docker nodes
docker node update --label-add usagi1=true <DOCKER-NODE1>
docker node update --label-add usagi2=true <DOCKER-NODE2>
docker node update --label-add usagi3=true <DOCKER-NODE3>

# Deploy the stack
docker stack deploy --compose-file=usagi.yml usagi-cluster
```

## Remove the cluster
Remove the rabbitmq cluster and related network using
```sh
docker stack rm usagi-cluster
```

Swarm doesn't automatically remove host volumes created for services. So run the following on each node.
```sh
docker volume prune
```

Enjoy 🤢

