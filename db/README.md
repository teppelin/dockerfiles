# DB

create 2 external netwoks - `db` & `prod`
```sh
docker network create --driver=overlay --attachable backend
```

## env vars
- `AUTH_USER_PASSWORD`
- `POSTGRES_PASSWORD`
- `POSTGRES_DB`
- `WEBDAV_USERNAME`
- `WEBDAV_PASSWORD`
- `WEBDAV_ENDPOINT`
- `AMQP_URI`
- `BRIDGE_CHANNELS` (`events:events,author:author,chapter:chapter,release_group:release_group,media:media,publisher:publisher,release:release,person:person`

